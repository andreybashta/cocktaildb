//
//  FiltersVC.swift
//  cocktaildb
//
//  Created by Andrey on 17.09.2020.
//  Copyright © 2020 Andrii Bashta. All rights reserved.
//

import UIKit

protocol FiltersDelegate: class {
    func didSelect(filtersIndices: [Int])
}

class FiltersVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var filters = [String]()
    var selectedIndices = [Int]()
    weak var filtersDelegate: FiltersDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         for (index, _) in filters.enumerated() {
            self.tableView.deselectRow(at: IndexPath(row: index, section: 0), animated: false)
        }
        
        for (_, element) in selectedIndices.enumerated() {
            tableView.selectRow(at: IndexPath(row: element, section: 0), animated: false, scrollPosition: UITableView.ScrollPosition(rawValue: 0)!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        let sorted = selectedIndices.sorted(by: <)
        filtersDelegate?.didSelect(filtersIndices: sorted)
    }

}

extension FiltersVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        filters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.Cell.filter.rawValue, for: indexPath) 
        cell.textLabel?.text = String(filters[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndices.append(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        for (index, element) in selectedIndices.enumerated() {
            if element == indexPath.row {
                selectedIndices.remove(at: index)
            }
        }
    }
    
}
