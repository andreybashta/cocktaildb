
import Foundation

//для раскодировки
struct Categories: Decodable {
    let value: [Category]
    
    enum CodingKeys: String, CodingKey {
        case value = "drinks"
    }
}

struct Category: Decodable, Hashable {
    let strCategory: String
    enum CodingKeys: String, CodingKey {
        case strCategory = "strCategory"
    }
}

//для отображения
struct CategoriesList {
    let category: Category
    var drinks: [Drink]
}
