
import Foundation

struct Drinks: Decodable {
    let value: [Drink]
    
    enum CodingKeys: String, CodingKey {
        case value = "drinks"
    }
}

struct Drink: Decodable {
    let strDrink: String
    let strDrinkThumb: String
    enum CodingKeys: String, CodingKey {
        case strDrink = "strDrink"
        case strDrinkThumb = "strDrinkThumb"
    }
}
