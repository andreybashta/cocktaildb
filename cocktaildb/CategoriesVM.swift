//
//  CategoriesVM.swift
//  cocktaildb
//
//  Created by Andrey on 17.09.2020.
//  Copyright © 2020 Andrii Bashta. All rights reserved.
//

import Foundation

protocol CategoriesViewModelDelegate: class {
    func onFetchCompleted()
    func onFetchFailed(with reason: String)
}

final class CategoriesViewModel {
    private weak var delegate: CategoriesViewModelDelegate?
    private(set) var data = [CategoriesList]()
    private(set) var currentLoadedSections = 0
    private(set) var selectedIndices = [Int]()
    let client = NetworkClient()
    
    init(delegate: CategoriesViewModelDelegate) {
        self.delegate = delegate
    }

    var categories: [Category] {
        data.map { $0.category }
    }

    func setSelectedIndices(with value: [Int]) {
        self.selectedIndices = value
    }
    
    func fetchCategories() {
        let request = R.Request.baseURL.rawValue + R.Request.categories.rawValue
        client.fetchData(with: request) { (result: Result<Categories, R.DataResponseError>) in
            switch result {
            case .failure(let error):
                DispatchQueue.main.async {
                    self.delegate?.onFetchFailed(with: error.reason)
                }
            case .success(let response):
                //Создаем ключи-категории
                var saved = 0
                for (index, element) in response.value.enumerated() {
                    self.data.insert(CategoriesList(category: element, drinks: []), at: index)
                    saved += 1
                    self.selectedIndices.append(index)
                    if saved == response.value.endIndex {
                        self.fetchDrinks(with: self.data[0].category, and: 0)
                    }
                }
            }
        }
    }
    
    func fetchDrinks(with category: Category, and categoryIndex: Int) {
        let request = (R.Request.baseURL.rawValue + R.Request.drinks.rawValue + category.strCategory).replacingOccurrences(of: " ", with: "_")
        client.fetchData(with: request) { (result: Result<Drinks, R.DataResponseError>) in
            switch result {
            case .failure(let error):
                DispatchQueue.main.async {
                    self.delegate?.onFetchFailed(with: error.reason)
                }
            case .success(let response):
                self.data[categoryIndex].drinks = response.value
                if self.categories.endIndex != self.currentLoadedSections {
                    self.currentLoadedSections += 1
                }
                    DispatchQueue.main.async {
                        self.delegate?.onFetchCompleted()
                    }
            }
        }
    }
    
//    private func calculateIndexPathsToReload(from newDrinks: [Drink]) -> [IndexPath] {
//        let startIndex = 0
//        let endIndex = startIndex + newDrinks.count
//        return (startIndex..<endIndex).map { IndexPath(row: $0, section: self.currentLoadedSections) }
//    }
    
}
