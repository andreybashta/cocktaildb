//
//  TableViewCell.swift
//  cocktaildb
//
//  Created by Andrey on 17.09.2020.
//  Copyright © 2020 Andrii Bashta. All rights reserved.
//

import UIKit
import Kingfisher

class CocktailCell: UITableViewCell {
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var cocktailImage: UIImageView!
    @IBOutlet weak var cocktailText: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        configure(with: .none)
        cocktailImage.kf.cancelDownloadTask()
        cocktailImage.image = nil
        indicatorView.alpha = 0
    }
    
    override func awakeFromNib() {
      super.awakeFromNib()
      indicatorView.hidesWhenStopped = true
        
    }
    
    func configure(with drink: Drink?) {
        if let drink = drink, let url = URL(string: drink.strDrinkThumb) {
            cocktailText.text = drink.strDrink
            cocktailImage.kf.setImage(with: url, completionHandler: { _ in
                self.setNeedsLayout()
            })
            cocktailImage.alpha = 1
            cocktailText.alpha = 1
            indicatorView.stopAnimating()
        } else {
            cocktailImage.alpha = 0
            cocktailText.alpha = 0
            indicatorView.startAnimating()
        }
    }
    
}
