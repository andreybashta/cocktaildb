//
//  FilterCell.swift
//  cocktaildb
//
//  Created by Andrey on 20.09.2020.
//  Copyright © 2020 Andrii Bashta. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        selectedBackgroundView = bgColorView
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            accessoryType = .checkmark
        } else {
            accessoryType = .none
        }
    }

}
