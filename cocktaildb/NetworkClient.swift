//
//  NetworkClient.swift
//  cocktaildb
//
//  Created by Andrey on 17.09.2020.
//  Copyright © 2020 Andrii Bashta. All rights reserved.
//

import Foundation

final class NetworkClient {
    let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func fetchData<T: Decodable>(with urlString: String, completion: @escaping (Result<T, R.DataResponseError>) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(.failure(.other(description: "Bad URL")))
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
                let decoder = JSONDecoder()
                if let items = try? decoder.decode(T.self, from: data) {
                    completion(.success(items))
                    return
                }
            }
            completion(.failure(.other(description: "Undefined error")))
        }.resume()
    }
    
}
