//
//  ViewController.swift
//  cocktaildb
//
//  Created by Andrey on 17.09.2020.
//  Copyright © 2020 Andrii Bashta. All rights reserved.
//

import UIKit

class ViewController: UIViewController, AlertDisplayer {
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    private var viewModel: CategoriesViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        indicatorView.startAnimating()
        tableView.isHidden = true
        tableView.dataSource = self
        tableView.delegate = self
        viewModel = CategoriesViewModel(delegate: self)
        viewModel.fetchCategories()
        
    }
    
    @IBAction func toFilters(_ sender: Any) {
        if let filtersController = self.storyboard?.instantiateViewController(withIdentifier: "FiltersVC") as? FiltersVC {
            filtersController.filtersDelegate = self
            filtersController.filters = viewModel.categories.map { $0.strCategory }
            filtersController.selectedIndices = viewModel.selectedIndices
            self.navigationController?.pushViewController(filtersController, animated: true)
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let categoryIndex = viewModel.selectedIndices[section]
        let data = viewModel.data[categoryIndex].drinks
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.Cell.cocktail.rawValue, for: indexPath) as! CocktailCell
        if isLoadingCell(for: indexPath) {
            cell.configure(with: .none)
        } else {
            let categoryIndex = viewModel.selectedIndices[indexPath.section]
            let data = viewModel.data[categoryIndex].drinks
            cell.configure(with: data[indexPath.row])
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.selectedIndices.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let categoryIndex = viewModel.selectedIndices[section]
        return viewModel.data[categoryIndex].category.strCategory
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if section == viewModel.currentLoadedSections - 1 {
            if section == viewModel.categories.count - 1 {
                //Проверка на последнюю секцию: если да, то запрос на напитки отправляется с индексом currentLoadedSections-1
                self.viewModel.fetchDrinks(with: viewModel.data[viewModel.currentLoadedSections - 1].category, and: viewModel.currentLoadedSections - 1)
            } else {
                self.viewModel.fetchDrinks(with: viewModel.data[viewModel.currentLoadedSections].category, and: viewModel.currentLoadedSections)
            }
        }
    }
}

extension ViewController: CategoriesViewModelDelegate {
    func onFetchCompleted() {
        indicatorView.stopAnimating()
        tableView.isHidden = false
        tableView.reloadData()
    }
    
    func onFetchFailed(with reason: String) {
        indicatorView.stopAnimating()
        let title = "Warning"
        let action = UIAlertAction(title: "OK", style: .default)
        displayAlert(with: title , message: reason, actions: [action])
    }
}



extension ViewController: FiltersDelegate {
    func didSelect(filtersIndices: [Int]) {
        self.viewModel.setSelectedIndices(with: filtersIndices)
        self.tableView.reloadData()
    }
}

private extension ViewController {
    func isLoadingCell(for indexPath: IndexPath) -> Bool {
        indexPath.section >= viewModel.categories.count
    }
}
