//
//  Resources.swift
//  cocktaildb
//
//  Created by Andrey on 17.09.2020.
//  Copyright © 2020 Andrii Bashta. All rights reserved.
//

import Foundation

enum R {
    enum Cell: String {
        case cocktail = "CocktailCell"
        case filter = "FilterCell"
    }
    enum Request: String {
      case baseURL = "https://www.thecocktaildb.com/api/json/v1/1/"
      case categories = "list.php?c=list"
      case drinks = "filter.php?c="
    }
    enum DataResponseError: Error {
      case network
      case decoding
      case other(description: String)
      
      var reason: String {
        switch self {
        case .network:
          return "An error occurred while fetching data "
        case .decoding:
          return "An error occurred while decoding data"
        default:
          return "Error"
        }
      }
    }
}
